---
layout: post
title:  "How I save money when monitoring background tasks"
date:   2021-12-13 07:00:00 +0200
categories: monitoring background cloud
---

After reviewing my monthly costs I was wondering if my monitoring was to expensive for the usage I currently need. I was using UptimeRobot until I came across Cronitor.

## Some context

The app I'm working on is [EagleStatus](https://eaglestatus.io).

EagleStatus checks and collects public incident updates from hundreds of services and notifies to a given Slack channel. This process is a background periodic task that cannot fail. As with everything, downtime happens, so at least if it goes down I want to be aware of this ASAP.

For monitoring background processes I'm using heartbeat monitoring. It consists of sending HTTP requests periodically to a monitoring service. If at some point no HTTP request has been received for some time (i.e, the heat stops beating), the monitoring service sends a notification to the specified channels (Slack, email, ...).

![uptimerobot picture]({{ site.url }}/assets/09-heartbeat.png)

_Heartbeat monitoring image_


## My first choice was UptimeRobot


[UptimeRobot](https://uptimerobot.com/) is a monitoring service that it is widely used. Originally, it was a solution to send HTTP pings to a given endpoint. This is very similar to heartbeats but changing the roles of sender and receiver. Anyhow, this solution is not convenient with background processes that do not need an HTTP server to operate.

![uptimerobot picture]({{ site.url }}/assets/04-uptimerobot-1.jpg)

_Uptimerobot monitor sample_

UptimeRobot recently offered the heartbeat solution. It works great, but the pricing plans are a bit excesive in my opinion. In order to have a single heartbeat integration I had to purchase a full 50 monitors plan.

![uptimerobot picture]({{ site.url }}/assets/05-uptimerobot-pricing.png)

_Uptimerobot pricing sample_


## Moving towards Cronitor

After searching for a solution that I could use with a fair free tier I came across [Cronitor](https://cronitor.io/).

![cronitor picture]({{ site.url }}/assets/06-cronitor.png)

_One of my EagleStatus monitors in Cronitor_


They offer multiple solutions for monitoring background processes as well as HTTP checks. I find that my background monitoring has much more observability since I started using Cronitor.

![cronitor picture]({{ site.url }}/assets/08-cronitor.png)
![cronitor picture]({{ site.url }}/assets/07-cronitor.png)

_Cronitor events and alerts logs_


Cronitor offers up to 5 monitors (including hearbeats) for free. This is great for projects as mine that need background process monitoring from the beginning.

![cronitor pricing]({{ site.url }}/assets/10-cronitor-pricing.png)

_Cronitor pricing sample_


I've been trying Cronitor for some time and the experience is very nice and support is great! I had a minor incident and had a great response by both of the co-founders (Shane and August).

If as me you need heartbeat or cron monitoring I suggest you give a try to Cronitor.


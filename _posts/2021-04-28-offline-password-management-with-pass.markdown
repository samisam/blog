---
layout: post
title:  "Pass, an offline password manager"
date:   2021-05-04 14:00:00 +0200
categories: cloud tech offline privacy
---

Earlier this year I was looking for an offline password manager and came across [Pass](https://www.passwordstore.org/)

_"Password management should be simple and follow Unix philosophy. With `pass`, each password lives inside of a `gpg` encrypted file whose filename is the title of the website or resource that requires the password. These encrypted files may be organized into meaningful folder hierarchies, copied from computer to computer, and, in general, manipulated using standard command line file management utilities."_


## Usage examples

- List existing passwords:
```sh
$ pass
Password Store
├── sapagat
│   ├── email
│   └── shopping-site.com
├── my-work
│   ├── email
│   └── some-work-site.com
└── my-project
    ├── sentry
    ├── gitlab
    └── heroku
```

- Generate a password:
```sh
$ pass generate sapagat/some-site.com
The generated password for sapagat/some-site.com is:
yE{PQPSCu(/{3HvKCIl:2=&6^
```

- Show a password:
```sh
$ pass sapagat/some-site.com
yE{PQPSCu(/{3HvKCIl:2=&6^
```

- Copy a password to the clipboard:
```sh
$ pass -c sapagat/some-site.com
Copied sapagat/some-site.com to clipboard. Will clear in 45 seconds.
```


## Main features I like

- Secure password generation
- Easy to copy to/from clipboard
- Encrypted on filesystem
- Easy to organize
- Additional data can be stored (links, second passwords, usernames, ...)
- Offline (very useful when playing with your router)
- No browser extensions, no privacy policy to dissect
- Stable solution (minimal dependencies)
- Unix-like auto-completions
- Portable


## Make it portable

Pass keeps all the passwords in individual files inside a directory in your home, `~/.password-store`.

With a symbolic link you can make Pass create the password store on a USB:

```sh
$ ln -s /media/sam/PASSWORDS/password-store ~/.password-store
```

![portable pass picture]({{ site.url }}/assets/02-pass-usb.jpg)

_The picture shows a portable password store in a small 2GB USB_



## Prepare for recovery

Since Pass creates a file structure on your computer, with a simple backup strategy you can go back in time and restore your passwords at any moment. I use [Timeshift](https://github.com/teejee2008/timeshift) as my work station backup strategy.

When having a portable password store, you can create a local backup each time the USB is mounted.

- Identify the device you want to backup:
    ```
    systemctl list-units -t mount
    ```

- Create a service in `systemd`:

    ```
    [Unit]
    Description=Passwords backup
    Requires=media-sam-PASSWORDS.mount
    After=media-sam-PASSWORDS.mount


    [Service]
    ExecStart=/home/sam/bin/backup-passwords

    [Install]
    WantedBy=media-sam-PASSWORDS.mount
    ```

- Customize your backup script:

    ```bash
    #!/bin/bash
    mkdir -p /home/sam/backups
    cp -r /media/sam/PASSWORDS/pass /home/sam/backups/
        $ ln -s /media/sam/PASSWORDS/password-store ~/.password-store
    ```


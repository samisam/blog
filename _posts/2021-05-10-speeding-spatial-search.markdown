---
layout: post
title:  "Speeding spatial search in JavaScript"
date:   2021-05-10 16:00:00 +0200
categories: data tech gis rbush
---

![network picture]({{ site.url }}/assets/03-network.jpg)

_Photo by [Alina Grubnyak](https://unsplash.com/@alinnnaaaa?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)._

At [Qatium](https://qatium.com), where I'm working now, we are facing very interesting challenges. One of them consists of finding what elements of a hydraulic network are close to each other. For instance, given 10k pipes ([GIS data](https://en.wikipedia.org/wiki/Geographic_information_system)) understand what pipes can be considered as _neighbors_.

This post gave us a push:

- [https://blog.mapbox.com/a-dive-into-spatial-search-algorithms-ebd0c5e39d2a](https://blog.mapbox.com/a-dive-into-spatial-search-algorithms-ebd0c5e39d2a)




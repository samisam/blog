---
layout: post
title:  "How I got flooded with exceptions and tried to manage it"
date:   2022-06-02 07:00:00 +0200
---

When your product depends on hundreds of third-parties and errors can occur every minute, raising an exception is not enough.

![wet floor image]({{ site.url }}/assets/wet-floor.jpg)

_Photo by [Oscar Sutton](https://unsplash.com/@o5ky?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)._

This is the case of [EagleStatus](https://eaglestatus.io), in which every minute it crawls many status pages to check if any incident has been reported. My first approach, as with other products, was to report an exception each time something went wrong.

This approach was quite bad.

## Some error scenarios

My initial approach was to raise an exception each time something unexpected occurred and receive an alert via Slack. Since status checks occur every minute, if an error persists during time I get flooded with error notifications and managing them is impossible.

These are the most common errors.


### Site is not reachable

This is the most common scenario. The status site of a given service is not reachable. It has different flavors:

- Not reachable for some seconds, minutes or hours
- Unstable (reachable -> not reachable -> reachable -> not reachable)
- Site permanently moved or removed

_Let's say a service is not reachable for 15 minutes. I would receive 15 exceptions._

### Provider site is not reachable

Most cloud services delegate their status sites to providers such as Statuspage, StatusIO and Instatus.
In addition, some monitoring services offer a status page based on their data. Some examples are UptimeRobot and Pingdom.

Providers normally have a robust infrastructure designed to avoid major outages. But, when given the circumstances, exceptions get multiplied.

_Let's say a provider is not reachable for 15 minutes and 100 sites are affected. I would receive 1500 exceptions._

### Site content has changed

In order to determine the status of some services, a crawler is needed (RSS and APIs are not always available).

Crawling is quite stable, but it is coupled to the site structure (HTML tags, CSS classes, texts, urls...). My first approach was to raise an exception when the content has changed.

Ordered by frequency these are some examples:

- A service adds a new service/component to their status page (f.e Pull Request and Packages are GitHub components)
- A new status is added (f.e a service decides to use OPERATIONAL instead of UP)
- Full site has changed.

_Let's say a site has changed and I take 2 hours to fix the changes, I would receive 120 exceptions._

But the problem gets even worst if a provider changes the site structure....

_Let's say a provider changes its site, 100 services are affected by this change and I take 2 hours to fix the changes, I would receive 12000 exceptions._


![explosion image]({{ site.url }}/assets/explosion.jpg)

_Photo by [Luke Jernejcic](https://unsplash.com/@jernejcic?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)._

## Towards a solution

Since the number of status checks is only going to grow, I'm in a continous search of a better solution. Here's what I've tried so far.

### Ingore errors

> "Oops. Let's say this error never happened."

In some cases this is a perfect solution, but in my case the service I provide is compromised if I'm not able to gather the current status. I need to know when things are not working as expected.

I tried ignoring errors for some time and the feeling of darkness made me jump to another solution.

### Use warning as severity level

> "Do not notify me about errors unless they persist during time."

Instead of reporting the exceptions as errors, Sentry (or similar solutions) offer the posibility to treat them as warnings. This allows you to configure alert settings such as "Notify me only after X number of warnings".

This solution reduces the number of alerts received but the errors are still in Sentry. Navigating the errors and understanding which one belongs to the alert is quite hard. In addition, it consumes your [errors budget](https://sentry.io/pricing/).

### Exponential Backoff

> "Not avaiable, now? Ok, I will try again next minute"

> "Not avaiable, now? Ok, I will try again 3 minute later"

> "Not avaiable, now? Ok, I will try again 9 minutes later"

[Exponential backoff](https://en.wikipedia.org/wiki/Exponential_backoff)

This solution adds some tolerance so that if an error persists for some time the number of alerts/exceptions received is reduced significantly.

The problem is if status site comes up again after some minutes of downtime and the backoff delay is big (30 minutes or so), there will be some time in which the current status is not being retrieved.

### Health endpoint

> "What status checks have failed for a long period?""

[Health endpoint pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/health-endpoint-monitoring)


In my case, a "healthy check" means that it has peformed successfully at least once during the last 15 minutes.

Here's how I implemented the solution:

- For each check, record if it went well or not and when it occured.
    ```
    CHECK_A SUCCESS 10:00
    CHECK_B FAILURE 10:00 REASON_X
    CHECK_C FAILURE 10:00 REASON_Y
    ...
    CHECK_A SUCCESS 10:01
    CHECK_B FAILURE 10:01 REASON_X
    CHECK_C SUCCESS 10:01
    ```
- Expose a health endpoint that lists the unhealthy checks.
    ```
    HTTP GET https://my-service/health

    {
        "status": "error",
        "unhealthyChecks": {
            "CHECK_B": { "lastSuccess": 09:45, "lastError": "REASON_X"}
        }
    }
    ```
- With a monitoring tool (f.e Prometheus, Uptimerobot or Cronitor), check periodically the health status. When the healh status is error, it sends a notification.

With this solution I can understand what needs attention and at the same time the service status can be retrieved as soon as the site is up again after some downtime (this was the main pain of exponential backoff).

### Things to keep improving

Currently I'm using the health endpoint pattern in production. Anyhow it has some space for improvement.

I'm using [Cronitor](https://cronitor.io/) to perform the health checks and to notify me when the status changes (ok -> error, error -> ok). Unfortunatelly it is not possible to include the reason in the notifications. So to understand why the system is unhealthy, I have to manually check the logs or access the endpoint from a browser. This can become problematic when I can't attend the unhealthy status on time.

In addtion, when multiple checks are failing, it is possible to miss a check that needs attention:

```
CHECK_A => UNHEALTHY      ---> UNHEALTHY NOTIFICATION
...
CHECK_B => UNHEALTHY
...
CHECK_B => HEALTHY
...
CHECK_A => HEALTHY        ---> HEALTHY NOTIFICATION
```

Last, but not least, sometimes there is nothing I can do. If a status site is not accessible and it has been like that for hours, I can check that the error is on their side and notify them. The problem is that my health endpoint will remain unhealthy until the issue is solved.

![example of third-party error]({{ site.url }}/assets/tweet-health-example.png)



## Conclusion

Although improvements can be made, the observability of my systems has improved a lot since I implemented the health endpoint pattern. When I recieve a health alert, I know what needs attention.

If you have any suggestion or similiar pains, feel free to reach out to me!


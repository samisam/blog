---
layout: post
title:  "How I replaced Google Drive"
date:   2021-04-27 08:52:06 +0200
categories: cloud home tech privacy
---

Earlier this year I decided to keep my personal files at home. The main challenge was to find an open-source, reliable and cheap solution to replace Google Drive. After trying with no success to replace it with an out-of-the-box solution, I figured out I had to break down my needs.


### Applications

It was quite easy to replace the Google Drive applications since I didn't need "multi-user collaboration" for my personal files. That's why with [LibreOffice](https://www.libreoffice.org/) was enough.


### Synchronization

In my case, I wanted access the same document from different devices. For instance, accessing my [Joplin](https://joplinapp.org/) notes from
my old laptop at the living room or from my work station at the office.

I solved this with [Syncthing](https://syncthing.net/). It is a simple P2P file synchornization solution. Set up is easy and file transfers are encrypted. I managed to [install](https://pimylifeup.com/raspberry-pi-syncthing/) it smoothly on an old [RaspberryPi 1B+](https://www.raspberrypi.org/products/raspberry-pi-1-model-b-plus/) (512MB RAM / 64GB USB storage, ~0.5W).

Cost: 15€ USB + old Raspi.

![raspi image]({{ site.url }}/assets/01-raspi-syncthing.jpg)

_RaspberryPi 1B+ with a mounted usb storage_

### Recovery

Recovery was my main concern. Suddently I was responsible of any of my data loss. As far as my situation, all the data I whish to preserve fits on a single hard drive and I normally access to it from my work station.

After writing down my requirements I came up with a very simple and obvious solution: perform periodic backups onto an external hard drive.

That's why I decided for [Timeshift](https://github.com/teejee2008/timeshift). It ensures backups are generated at least on a daily basis without manual intervention. Its main feature is that it allows you to go back in time when something goes wrong.

Cost: 50€ hard drive (2TB size were enough for my needs).

![timeshift sample image]({{ site.url }}/assets/01-timeshift-sample.png)

_Timeshift ensures backups from different intervals_

### Other solutions discarded

- [Nextcloud](https://nextcloud.com/): it is complex and solves more problems than what I really need.
- [OpenMediaVault](https://www.openmediavault.org/): although promising, I was struggling to make it work smothly on a RaspberryPi.
- DejaDup (default backups software in Ubuntu). I've experience a buggy behavior and found this on [Reddit](https://www.reddit.com/r/Ubuntu/comments/4blx2o/should_we_replace_deja_dup_as_the_default_backup/)


---
layout: post
title:  "My first blog post"
date:   2021-04-26 17:52:06 +0200
categories: first welcome
---

Welcome!

After many years of consuming content on the Intenet I've figured it's my moment to start sharing some of my experiences.

Although I have no previous experence with blogging, I believe it's the most personal way of sharing content and at the same time the most private way of consuming it. That's why I believe it is worth the struggle.

Thanks for reading, I'll try my best!
